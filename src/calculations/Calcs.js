import React, { Component } from 'react';
import './Calcs.css';

function factorial(n) {
    
    if (n <= 1 ){
        return 1;
    }
    
    return n * factorial(n-1);
}

class Calcs extends Component {
  
    constructor(props){
        super(props);
        this.state = {value: 0, error: null};
    }
  
    handleChange(event) {
        this.setState({value: event.target.value});
    }

    render() {
        if (this.state.error) {
            return <h1>Caught an error.</h1>
        }

        return (
            <div className="calcsDiv">
                <h2>Big-O Calculator</h2>
                <label>Enter 'n' value:</label>
                <input type="number" value={this.state.value} onChange={this.handleChange.bind(this)} />
                <p><strong>Constant:</strong> <span>1</span></p>
                <p><strong>Logarithmic:</strong> <span>{Math.log2(this.state.value)}</span></p>
                <p><strong>Linear:</strong> <span>{this.state.value}</span></p>
                <p><strong>Quadratic:</strong> <span>{this.state.value * this.state.value}</span></p>
                <p><strong>Exponential:</strong> <span>{Math.pow(2, this.state.value)}</span></p>
                <p><strong>Factorial:</strong> <span>{factorial(this.state.value)}</span></p>
            </div>
        );
    }
}

export default Calcs;