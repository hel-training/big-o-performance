import React, {Component} from 'react';
import './Charts.css';
import graphsData from "./GraphsData";

const LineChart = require("react-chartjs-2").Line;

class Charts extends Component {

    render() {
        const chartOptions = {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        };

        return (
            <div className="chartsDiv">
                <h2>Big-O Graphs</h2>

                <div className="row">
                    <div className="column">
                        <h3>O(1) - Constant</h3>
                        <LineChart className="Chart" data={graphsData.constantData} options={chartOptions} width="400" height="200"/>
                    </div>
                    <div className="column">
                        <h3>O(log n) - Logarithmic</h3>
                        <LineChart className="Chart" data={graphsData.logData} options={chartOptions} width="400" height="200"/>
                    </div>

                </div>

                <div className="row">
                    <div className="column">
                        <h3>O(n) - Linear</h3>
                        <LineChart className="Chart" data={graphsData.linearData} options={chartOptions} width="400" height="200"/>
                    </div>

                    <div className="column">
                        <h3>O(n^2) - Quadratic</h3>
                        <LineChart className="Chart" data={graphsData.quadData} options={chartOptions} width="400" height="200"/>
                    </div>
                </div>

                <div className="row">
                    <div className="column">
                        <h3>O(c^n) - Exponential</h3>
                        <LineChart className="Chart" data={graphsData.expData} options={chartOptions} width="400" height="200"/>
                    </div>

                    <div className="column">
                        <h3>O(n!) - Factorial</h3>
                        <LineChart className="Chart" data={graphsData.factData} options={chartOptions} width="400" height="200"/>
                    </div>
                </div>
            </div>);
    }
}

export default Charts;