import Calcs from '../calculations/Calcs';
import Charts from '../charts/Charts';
import './App.css';
import { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Big-O Notation Analyzer</h2>
        </div>
        <div className="App-left">
          <Calcs />
        </div>
        <div className="App-right">
          <Charts />
        </div>
      </div>
    );
  }
}

export default App;
